public class PassengerPlane {
    private final String model;
    private final int yearMade;
    private int seat;
    private double price;

    public PassengerPlane(String model, int yearMade, int seat, double price) {
        this.model = model;
        this.yearMade = yearMade;
        this.seat = seat;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public int getYearMade() {
        return yearMade;
    }

    public int getSeat() {
        return seat;
    }

    public double getPrice() {
        return price;
    }
}
