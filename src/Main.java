public class Main {
    public static void main(String[] args) {
        PlaneCollection collection = new PlaneCollection();

        PassengerPlane plan1 = new PassengerPlane("A330", 2000, 200, 100000);
        PassengerPlane plan2 = new PassengerPlane("Boeing 747", 1960, 400, 500000);
        PassengerPlane plan3 = new PassengerPlane("Boeing 777", 1980, 300, 200000);

        collection.addPassengerPlane(plan1);
        collection.addPassengerPlane(plan2);
        collection.addPassengerPlane(plan3);

    }
}
