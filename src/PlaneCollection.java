import java.util.ArrayList;

public class PlaneCollection {
    private final ArrayList<PassengerPlane> planeList;

    public PlaneCollection() {
        planeList = new ArrayList<>();
    }

    public void addPassengerPlane(PassengerPlane passengerPlane) {
        assert passengerPlane !=null;
        planeList.add(passengerPlane);
    }

    public void removePassengerPlane(PassengerPlane passengerPlane){
        assert passengerPlane != null;
        if (planeList.contains(passengerPlane)) {
            planeList.remove(passengerPlane);
        }
    }

}
